const express = require('express');
const router = express.Router();

// > Import Controllers
const userController = require('../controllers').user;
const profileController = require('../controllers').profile;
const companyController = require('../controllers').company;
const branchController = require('../controllers').branch;
const roleController = require('../controllers').role;

router.get('/', (req, res) => {
  res.status(200).json({
    status: 'Success',
    statusCode: 200,
    message: 'Hello World!'
  });
});

// User Router
router.get('/api/v1/user/find-all', userController.findAllUser);
router.get('/api/v1/user/find/:id', userController.getUserById);
router.post('/api/v1/user/add', userController.addUser);
router.post('/api/v1/user/add-with-profile', userController.addUserWithProfile);
router.put('/api/v1/user/update/:id', userController.updateUser);
router.delete('/api/v1/user/delete/:id', userController.deleteUser);

// Profile Router
router.get('/api/v1/profile/find-all', profileController.findAllProfile);
router.get('/api/v1/profile/find/:id', profileController.getProfileById);
router.post('/api/v1/profile/add', profileController.addProfile);
router.put('/api/v1/profile/update/:id', profileController.updateProfile);
router.delete('/api/v1/profile/delete/:id', profileController.deleteProfile);

// Company Router
router.get('/api/v1/company/find-all', companyController.findAllCompany);
router.get('/api/v1/company/find/:id', companyController.getCompanyById);
router.post('/api/v1/company/add', companyController.addCompany);
router.put('/api/v1/company/update/:id', companyController.updateCompanyWithBranches);
router.delete('/api/v1/company/delete/:id', companyController.deleteCompany);

// Branch Router
router.get('/api/v1/branch/find-all', branchController.findAllBranch);
router.get('/api/v1/branch/find/:id', branchController.getBranchById);
router.post('/api/v1/branch/add', branchController.addBranch);
router.put('/api/v1/branch/update/:id', branchController.updateBranch);
router.delete('/api/v1/branch/delete/:id', branchController.deleteBranch);

// Role Router
router.get('/api/v1/role/find-all', roleController.findAllRole);
router.get('/api/v1/role/find/:id', roleController.getRoleById);
router.post('/api/v1/role/add', roleController.addRole);
router.put('/api/v1/role/update/:id', roleController.updateRole);
router.delete('/api/v1/role/delete/:id', roleController.deleteRole);

// Advanced Router
router.post('/api/v1/company/add-with-branch', companyController.addCompanyWithBranches);
router.post('/api/v1/role/add-user-role', roleController.addUserWithRole);

module.exports = router;