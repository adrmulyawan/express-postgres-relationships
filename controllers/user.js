const models = require('../models');
const User = models.User;
const Profile = models.Profile;
const Role = models.Role;
const UserRole = models.UserRole;

const findAllUser = async (req, res) => {
  try {
    const items = await User.findAll({
      include: ['profile', 'roles'],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data is Found!',
      data: items.length < 1 ? 'Data User is Empty' : items,
    });
  } catch (error) {
    console.info(error);
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const getUserById = async (req, res) => {
  try {
    const data = await User.findByPk(req.params.id, {
      include: ['profile', 'roles'],
    });

    if (!data) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data User is Not Found!'
      });
    }

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data User Found!',
      data: data,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addUser = async (req, res) => {
  try {
    const newUser = await User.create({
      username: req.body.username,
      password: req.body.password,
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New User!',
      data: newUser,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addUserWithProfile = async (req, res) => {
  try {
    const newUser = await User.create({
      username: req.body.username,
      password: req.body.password,
      profile: req.body.profile,
    }, { 
      include: ['profile']
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New User and Profile!',
      data: newUser,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const updateUser = async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id, {
      include: ['profile', 'roles'],
    });

    if (!user) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data User is Not Found!'
      });
    }

    try {
      const update = await user.update({
        username: req.body.username || user.username,
        password: req.body.password || user.password,
      });

      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Update Data User!',
        data: update,
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const deleteUser = async (req, res) => {
  try {
    const user = await User.findByPk(req.params.id);

    if (!user) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data User is Not Found!'
      });
    }

    try {
      await user.destroy();

      return res.status(204).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Delete Data User!',
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

module.exports = {
  findAllUser,
  getUserById,
  addUser,
  addUserWithProfile,
  updateUser,
  deleteUser,
};