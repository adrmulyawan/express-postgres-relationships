const models = require('../models');
const Branch = models.Branch;
const Company = models.Company;

const findAllBranch = async (req, res) => {
  try {
    const items = await Branch.findAll({
      include: ['company'],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Branch is Found!',
      data: items,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const getBranchById = async (req, res) => {
  try {
    const data = await Branch.findByPk(req.params.id, {
      include: ['company'],
    });

    if (!data) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Branch is Not Found!',
      });
    }

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Branch is Found!',
      data: data,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addBranch = async (req, res) => {
  try {
    const branch = await Branch.create({
      company_id: req.body.company_id,
      branch_name: req.body.branch_name,
      branch_address: req.body.branch_address,
      branch_city: req.body.branch_city,
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New Branch!',
      data: branch,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const updateBranch = async (req, res) => {
  try {
    const branch = await Branch.findByPk(req.params.id, {
      include: ['company'],
    });

    if (!branch) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Branch is Not Found!',
      });
    }

    try {
      const updateBranch = await branch.update({
        branch_name: req.body.branch_name || branch.branch_name,
        branch_address: req.body.branch_address || branch.branch_address,
        branch_city: req.body.branch_city || branch.branch_city,
      });

      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Update Data Branch!',
        data: updateBranch,
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const deleteBranch = async (req, res) => {
  try {
    const branch = await Branch.findByPk(req.params.id);

    if (!branch) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Branch is Not Found!',
      });
    }

    try {
      await branch.destroy();

      return res.status(204).json({
        status: 'Success',
        statusCode: 204,
        message: 'Success Delete Data Branch',
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

module.exports = {
  findAllBranch,
  getBranchById,
  addBranch,
  updateBranch,
  deleteBranch,
};