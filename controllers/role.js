const models = require('../models');
const User = models.User;
const Role = models.Role;

const findAllRole = async (req, res) => {
  try {
    const items = await Role.findAll({
      include: ['users'],
      order: [
        ['createdAt', 'DESC']
      ]
    });

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data is Found!',
      data: items.length < 1 ? 'Data Role is Empty!' : items,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const getRoleById = async (req, res) => {
  try {
    const data = await Role.findByPk(req.params.id, {
      include: ['users']
    });

    if (!data) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Role is Not Found!'
      });
    }

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Role is Found!',
      data: data,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addRole = async (req, res) => {
  try {
    const data = await Role.create({
      role_name: req.body.role_name,
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New Role!',
      data: data,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addUserWithRole = async (req, res) => {
  try {
    // > Cari role_id yang id = id role
    const role = await Role.findOne({
      where: {
        id: req.body.id
      },
      include: ['users']
    });
    
    // > Apabila tidak ditemukan return 404 (Not Found)
    if (!role) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Role is Not Found!'
      });
    }

    try {
      // > Cari role_id yang id = id user
      const user = await User.findOne({
        where: {
          id: req.body.id
        }
      });

      // > Apabila tidak ditemukan return 404 (Not Found)
      if (!user) {
        return res.status(404).json({
          status: 'Failed',
          statusCode: 404,
          message: 'Sorry, Data User is Not Found!'
        });
      }

      // > Jika ditemukan tambahkan user kedalam role
      role.addUser(user);

      // > Return (tampilkan) pesan berhasil
      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Add User to Role',
        data: role,
      });
    } catch (error) {
      // > Jika error (terjadi kesalahan)
      console.info(error);
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    // > Jika error (terjadi kesalahan)
    console.info(error);
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const updateRole = async (req, res) => {
  try {
    // > Cari data role berdasarkan primary key
    // => Assign ke variable role
    const role = await Role.findByPk(req.params.id, {
      include: ['users'],
    });

    // > Apabila tidak ditemukan return 404 (Not Found)
    if (!role) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data User is Not Found!'
      });
    }

    try {
      // > Jika ditemukan update datanya 
      const updateRole = await role.update({
        role_name: req.body.role_name || role.role_name,
      });

      // > Return response bila data berhasil diupdate
      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Update Data Role!',
        data: updateRole
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const deleteRole = async (req, res) => {
  try {
    const role = await Role.findByPk(req.params.id);

    if (!role) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data User is Not Found!'
      });
    }

    try {
      await role.destroy()

      res.status(204).json({
        status: 'Success',
        statusCode: 204,
        message: 'Success Delete Data Role!'
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

module.exports = {
  findAllRole,
  getRoleById,
  addRole,
  addUserWithRole,
  updateRole,
  deleteRole
};