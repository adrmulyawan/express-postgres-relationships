const models = require('../models');
const User = models.User;
const Profile = models.Profile;

const findAllProfile = async (req, res) => {
  try {
    const items = await Profile.findAll({
      include: ['user'],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Profile is Found!',
      data: items.length < 1 ? 'Sorry, Data Profile is Empty' : items
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const getProfileById = async (req, res) => {
  try {
    const data = await Profile.findByPk(req.params.id, {
      include: ['user'],
    });

    if (!data) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Profile is Not Found!',
      });
    }

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Profile is Found!',
      data: data,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addProfile = async (req, res) => {
  try {
    const profile = await Profile.create({
      user_id: req.body.user_id,
      full_name: req.body.full_name,
      birthdate: req.body.birthdate,
      gender: req.body.gender,
      position: req.body.position,
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New Data Profile!',
      data: profile,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const updateProfile = async (req, res) => {
  try {
    const profile = await Profile.findByPk(req.params.id, {
      include: ['user'],
    });

    if (!profile) {
      return res.status(404).json({
        status: 'Success',
        statusCode: 404,
        message: 'Sorry, Data Profile is Not Found!',
      });
    }

    try {
      const update = await profile.update({
        user_id: req.body.user_id || profile.user_id,
        full_name: req.body.full_name || profile.full_name,
        birthdate: req.body.birthdate || profile.birthdate,
        gender: req.body.gender || profile.gender,
        position: req.body.position || profile.position,
      });

      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Update Data Profile!',
        data: update,
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const deleteProfile = async (req, res) => {
  try {
    const profile = await Profile.findByPk(req.params.id);

    if (!profile) {
      return res.status(404).json({
        status: 'Success',
        statusCode: 404,
        message: 'Sorry, Data Profile is Not Found!',
      });
    }

    try {
      await profile.destroy();

      return res.status(204).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Delete Data Profile!',
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

module.exports = {
  findAllProfile,
  getProfileById,
  addProfile,
  updateProfile,
  deleteProfile
};