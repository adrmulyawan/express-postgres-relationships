const models = require('../models');
const Company = models.Company;
const Branch = models.Branch;

const findAllCompany = async (req, res) => {
  try {
    const items = await Company.findAll({
      include: ['branches'],
      order: [
        ['createdAt', 'DESC'],
      ],
    });

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Company is Found!',
      data: items.length < 1 ? 'Sorry, Data Company is Not Found!' : items,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const getCompanyById = async (req, res) => {
  try {
    const data = await Company.findByPk(req.params.id, {
      include: ['branches'],
    });

    if (!data) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Company is Not Found!',
      });
    }

    return res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Company is Found!',
      data: data,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addCompany = async (req, res) => {
  try {
    const company = await Company.create({
      company_name: req.body.company_name,
      company_address: req.body.company_address,
      company_city: req.body.company_city,
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New Data Company!',
      data: company,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const addCompanyWithBranches = async (req, res) => {
  try {
    const company = await Company.create({
      company_name: req.body.company_name,
      company_address: req.body.company_address,
      company_city: req.body.company_city,
      branches: req.body.branches,
    }, { 
      include: ['branches'] 
    });

    return res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success Add New Data Company!',
      data: company,
    });
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const updateCompanyWithBranches = async (req, res) => {
  try {
    // const company = await Company.findOne(req.params.id, {
    //   include: ['branches'],
    // });

    const company = await Company.findOne({
      where: {
        id: req.params.id,
      },
      include: ['branches'],
    });

    if (!company) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Company is Not Found!',
      });
    }

    try {
      const update = await company.update({
        company_name: req.body.company_name || company.company_name,
        company_address: req.body.company_address || company.company_address,
        company_city: req.body.company_city || company.company_city,
        branches: req.body.branches || company.branches,
      }, {
        include: ['branches'],
      });

      return res.status(201).json({
        status: 'Success',
        statusCode: 201,
        message: 'Success Update Data Company!',
        data: update
      });
    } catch (error) {
      console.info(error);
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

const deleteCompany = async (req, res) => {
  try {
    const company = await Company.findByPk(req.params.id);

    if (!company) {
      return res.status(404).json({
        status: 'Failed',
        statusCode: 404,
        message: 'Sorry, Data Company is Not Found!',
      });
    }

    try {
      await company.destroy();

      return res.status(204).json({
        status: 'Success',
        statusCode: 204,
        message: 'Success Delete Data Company'
      });
    } catch (error) {
      return res.status(400).json({
        status: 'Failed',
        statusCode: 400,
        message: error,
      });
    }
  } catch (error) {
    return res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: error,
    });
  }
};

module.exports = {
  findAllCompany,
  getCompanyById,
  addCompany,
  addCompanyWithBranches,
  updateCompanyWithBranches,
  deleteCompany,
};