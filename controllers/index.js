const user = require('./user');
const profile = require('./profile');
const company = require('./company');
const branch = require('./branch');
const role = require('./role');

module.exports = {
  user,
  profile,
  company,
  branch,
  role,
};